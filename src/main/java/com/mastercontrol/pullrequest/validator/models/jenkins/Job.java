package com.mastercontrol.pullrequest.validator.models.jenkins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Job {
    public LastBuild lastBuild;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class LastBuild {
        public String number;
    }
}
