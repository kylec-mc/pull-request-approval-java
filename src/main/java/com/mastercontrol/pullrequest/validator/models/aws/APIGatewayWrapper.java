package com.mastercontrol.pullrequest.validator.models.aws;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mastercontrol.pullrequest.validator.models.bitbucket.ApprovalRequest;
import com.mastercontrol.pullrequest.validator.models.bitbucket.WebhookHeaders;
import lombok.Data;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class APIGatewayWrapper {
    private Map<String, String> headers;
    private String body;
}
