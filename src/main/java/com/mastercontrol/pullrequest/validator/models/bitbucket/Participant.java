package com.mastercontrol.pullrequest.validator.models.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Participant {
    public User user;
    public Boolean approved;
}
