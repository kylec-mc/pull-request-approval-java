package com.mastercontrol.pullrequest.validator.models.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class WebhookHeaders {
    @JsonProperty("X-Request-UUID")
    private String requestUuid;

    @JsonProperty("X-Event-Key")
    private String eventKey;

    @JsonProperty("User-Agent")
    private String userAgent;

    @JsonProperty("X-Attempt-Number")
    private int attempNumber;

    @JsonProperty("X-Hook-UUID")
    private String hookUuid;

    @JsonProperty("Content-Type")
    private String contentType;
}
