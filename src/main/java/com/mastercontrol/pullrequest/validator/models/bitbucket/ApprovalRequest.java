package com.mastercontrol.pullrequest.validator.models.bitbucket;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApprovalRequest {

    public Approval approval;
    public User actor;

    public PullRequestData pullrequest;

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class PullRequestData {
        public User author;
        public List<Participant> participants;
        public String id;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Approval {
        public User user;
    }
}
