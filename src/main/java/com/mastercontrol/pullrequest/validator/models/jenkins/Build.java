package com.mastercontrol.pullrequest.validator.models.jenkins;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Build {
    public boolean building;
}
