package com.mastercontrol.pullrequest.validator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercontrol.pullrequest.validator.models.aws.APIGatewayWrapper;
import com.mastercontrol.pullrequest.validator.models.bitbucket.ApprovalRequest;
import com.mastercontrol.pullrequest.validator.models.bitbucket.User;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class Handler implements RequestHandler<APIGatewayWrapper, ApiGatewayResponse> {

	private static final Logger LOG = Logger.getLogger(Handler.class);

	@Override
	public ApiGatewayResponse handleRequest(APIGatewayWrapper input, Context context) {
		LOG.info("received: " + input);
		LOG.info("Very beginning");
		//Response responseBody = new Response("Go Serverless v1.x! Your function executed successfully! Let's build this", input);

		//notifyTheButler(input.get("body").toString(), input.get("headers"));
		LOG.info("About to check if needed");
		buildIfNeeded(input.getBody(), input.getHeaders());
		LOG.info("About to return");
		return ApiGatewayResponse.builder()
				.setStatusCode(200)
				.setObjectBody(input.getBody())
				.setHeaders(Collections.singletonMap("X-Powered-By", "AWS Lambda & serverless"))
				.build();
	}

	@SneakyThrows(IOException.class)
	private void buildIfNeeded(String body, Map<String, String> headers) {
		ObjectMapper objectMapper = new ObjectMapper();
		ApprovalRequest approvalRequest = objectMapper.readValue(body, ApprovalRequest.class);
		LOG.info("Marshalled things");

		if (shouldBuild(approvalRequest)) {
			LOG.info("Decided we should build");
			notifyTheButler(body, headers);
		} else {
			System.out.println("We don't build author approvals");
		}

	}

	private boolean shouldBuild(ApprovalRequest approvalRequest) {
		User pullRequestAuthor = approvalRequest.pullrequest.author;

		return !pullRequestAuthor.getUsername().equals(approvalRequest.approval.getUser().getUsername());
	}

	private void notifyTheButler(final String responseBody, final Map<String, String> headers){
		//String url = "http://jenkins.mainman.dcs:8080/bitbucket-scmsource-hook/notify";
		LOG.info("Entered notify the butler");
		String url = System.getenv("JENKINS_HOOK_URL");


		headers.put("X-Event-Key", "pullrequest:updated");

		try {
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			headers.forEach(connection::setRequestProperty);
			LOG.info("About to send request");
			OutputStream out = connection.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "utf-8"));
			writer.write(responseBody);
			writer.flush();
			writer.close();
			out.close();
			LOG.info("Request Sent");

			System.out.println(connection.getResponseCode());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
